package tech.codingzen.ktuple

data class KTuple4<out P0, out P1, out P2, out P3>(
  val part0: P0,
  val part1: P1,
  val part2: P2,
  val part3: P3
)

fun <P0, P1, P2, P3, Q0> KTuple4<P0, P1, P2, P3>.set0(q0: Q0): KTuple4<Q0, P1, P2, P3> =
  KTuple4(q0, part1, part2, part3)

fun <P0, P1, P2, P3, Q1> KTuple4<P0, P1, P2, P3>.set1(q1: Q1): KTuple4<P0, Q1, P2, P3> =
  KTuple4(part0, q1, part2, part3)

fun <P0, P1, P2, P3, Q2> KTuple4<P0, P1, P2, P3>.set2(q2: Q2): KTuple4<P0, P1, Q2, P3> =
  KTuple4(part0, part1, q2, part3)

fun <P0, P1, P2, P3, Q3> KTuple4<P0, P1, P2, P3>.set3(q3: Q3): KTuple4<P0, P1, P2, Q3> =
  KTuple4(part0, part1, part2, q3)

inline fun <P0, P1, P2, P3, Q0> KTuple4<P0, P1, P2, P3>.map0(fn: (P0) -> Q0): KTuple4<Q0, P1,
  P2, P3> = KTuple4(fn(part0), part1, part2, part3)

inline fun <P0, P1, P2, P3, Q1> KTuple4<P0, P1, P2, P3>.map1(fn: (P1) -> Q1): KTuple4<P0, Q1,
  P2, P3> = KTuple4(part0, fn(part1), part2, part3)

inline fun <P0, P1, P2, P3, Q2> KTuple4<P0, P1, P2, P3>.map2(fn: (P2) -> Q2): KTuple4<P0, P1,
  Q2, P3> = KTuple4(part0, part1, fn(part2), part3)

inline fun <P0, P1, P2, P3, Q3> KTuple4<P0, P1, P2, P3>.map3(fn: (P3) -> Q3): KTuple4<P0, P1,
  P2, Q3> = KTuple4(part0, part1, part2, fn(part3))
