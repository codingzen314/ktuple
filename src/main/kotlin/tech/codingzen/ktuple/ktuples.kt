package tech.codingzen.ktuple

object KTuple {
  operator fun <P0> invoke(part0: P0): KTuple1<P0> = KTuple1(part0)

  operator fun <P0, P1> invoke(part0: P0, part1: P1): KTuple2<P0, P1> = KTuple2(part0, part1)

  operator fun <P0, P1, P2> invoke(
    part0: P0,
    part1: P1,
    part2: P2
  ): KTuple3<P0, P1, P2> = KTuple3(part0, part1, part2)

  operator fun <P0, P1, P2, P3> invoke(
    part0: P0,
    part1: P1,
    part2: P2,
    part3: P3
  ): KTuple4<P0, P1, P2, P3> = KTuple4(part0, part1, part2, part3)

  operator fun <P0, P1, P2, P3, P4> invoke(
    part0: P0,
    part1: P1,
    part2: P2,
    part3: P3,
    part4: P4
  ): KTuple5<P0, P1, P2, P3, P4> = KTuple5(part0, part1, part2, part3, part4)

  operator fun <P0, P1, P2, P3, P4, P5> invoke(
    part0: P0,
    part1: P1,
    part2: P2,
    part3: P3,
    part4: P4,
    part5: P5
  ): KTuple6<P0, P1, P2, P3, P4, P5> = KTuple6(part0, part1, part2, part3, part4, part5)

  operator fun <P0, P1, P2, P3, P4, P5, P6> invoke(
    part0: P0,
    part1: P1,
    part2: P2,
    part3: P3,
    part4: P4,
    part5: P5,
    part6: P6
  ): KTuple7<P0, P1, P2, P3, P4, P5, P6> = KTuple7(part0, part1, part2, part3, part4, part5, part6)

  operator fun <P0, P1, P2, P3, P4, P5, P6, P7> invoke(
    part0: P0,
    part1: P1,
    part2: P2,
    part3: P3,
    part4: P4,
    part5: P5,
    part6: P6,
    part7: P7
  ): KTuple8<P0, P1, P2, P3, P4, P5, P6, P7> = KTuple8(
    part0, part1, part2, part3, part4, part5,
    part6, part7
  )
}
