package tech.codingzen.ktuple

data class KTuple8<out P0, out P1, out P2, out P3, out P4, out P5, out P6, out P7>(
  val part0: P0,
  val part1: P1,
  val part2: P2,
  val part3: P3,
  val part4: P4,
  val part5: P5,
  val part6: P6,
  val part7: P7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q0> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set0(q0: Q0): KTuple8<Q0, P1, P2, P3, P4, P5, P6, P7> = KTuple8(
  q0, part1, part2, part3,
  part4, part5, part6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q1> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set1(q1: Q1): KTuple8<P0, Q1, P2, P3, P4, P5, P6, P7> = KTuple8(
  part0, q1, part2, part3,
  part4, part5, part6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q2> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set2(q2: Q2): KTuple8<P0, P1, Q2, P3, P4, P5, P6, P7> = KTuple8(
  part0, part1, q2, part3,
  part4, part5, part6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q3> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set3(q3: Q3): KTuple8<P0, P1, P2, Q3, P4, P5, P6, P7> = KTuple8(
  part0, part1, part2, q3,
  part4, part5, part6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q4> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set4(q4: Q4): KTuple8<P0, P1, P2, P3, Q4, P5, P6, P7> = KTuple8(
  part0, part1, part2, part3,
  q4, part5, part6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q5> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set5(q5: Q5): KTuple8<P0, P1, P2, P3, P4, Q5, P6, P7> = KTuple8(
  part0, part1, part2, part3,
  part4, q5, part6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q6> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set6(q6: Q6): KTuple8<P0, P1, P2, P3, P4, P5, Q6, P7> = KTuple8(
  part0, part1, part2, part3,
  part4, part5, q6, part7
)

fun <P0, P1, P2, P3, P4, P5, P6, P7, Q7> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.set7(q7: Q7): KTuple8<P0, P1, P2, P3, P4, P5, P6, Q7> = KTuple8(
  part0, part1, part2, part3,
  part4, part5, part6, q7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q0> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map0(fn: (P0) -> Q0): KTuple8<Q0, P1, P2, P3, P4, P5, P6, P7> = KTuple8(
  fn(part0), part1,
  part2, part3, part4, part5, part6, part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q1> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map1(fn: (P1) -> Q1): KTuple8<P0, Q1, P2, P3, P4, P5, P6, P7> = KTuple8(
  part0, fn(part1),
  part2, part3, part4, part5, part6, part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q2> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map2(fn: (P2) -> Q2): KTuple8<P0, P1, Q2, P3, P4, P5, P6, P7> = KTuple8(
  part0, part1,
  fn(part2), part3, part4, part5, part6, part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q3> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map3(fn: (P3) -> Q3): KTuple8<P0, P1, P2, Q3, P4, P5, P6, P7> = KTuple8(
  part0, part1, part2,
  fn(part3), part4, part5, part6, part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q4> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map4(fn: (P4) -> Q4): KTuple8<P0, P1, P2, P3, Q4, P5, P6, P7> = KTuple8(
  part0, part1, part2,
  part3, fn(part4), part5, part6, part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q5> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map5(fn: (P5) -> Q5): KTuple8<P0, P1, P2, P3, P4, Q5, P6, P7> = KTuple8(
  part0, part1, part2,
  part3, part4, fn(part5), part6, part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q6> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map6(fn: (P6) -> Q6): KTuple8<P0, P1, P2, P3, P4, P5, Q6, P7> = KTuple8(
  part0, part1, part2,
  part3, part4, part5, fn(part6), part7
)

inline fun <P0, P1, P2, P3, P4, P5, P6, P7, Q7> KTuple8<P0, P1, P2, P3, P4, P5, P6,
  P7>.map7(fn: (P7) -> Q7): KTuple8<P0, P1, P2, P3, P4, P5, P6, Q7> = KTuple8(
  part0, part1, part2,
  part3, part4, part5, part6, fn(part7)
)
