package tech.codingzen.ktuple

data class KTuple3<out P0, out P1, out P2>(
  val part0: P0,
  val part1: P1,
  val part2: P2
)

fun <P0, P1, P2, Q0> KTuple3<P0, P1, P2>.set0(q0: Q0): KTuple3<Q0, P1, P2> = KTuple3(
  q0,
  part1, part2
)

fun <P0, P1, P2, Q1> KTuple3<P0, P1, P2>.set1(q1: Q1): KTuple3<P0, Q1, P2> = KTuple3(
  part0,
  q1, part2
)

fun <P0, P1, P2, Q2> KTuple3<P0, P1, P2>.set2(q2: Q2): KTuple3<P0, P1, Q2> = KTuple3(
  part0,
  part1, q2
)

inline fun <P0, P1, P2, Q0> KTuple3<P0, P1, P2>.map0(fn: (P0) -> Q0): KTuple3<Q0, P1, P2> =
  KTuple3(fn(part0), part1, part2)

inline fun <P0, P1, P2, Q1> KTuple3<P0, P1, P2>.map1(fn: (P1) -> Q1): KTuple3<P0, Q1, P2> =
  KTuple3(part0, fn(part1), part2)

inline fun <P0, P1, P2, Q2> KTuple3<P0, P1, P2>.map2(fn: (P2) -> Q2): KTuple3<P0, P1, Q2> =
  KTuple3(part0, part1, fn(part2))
