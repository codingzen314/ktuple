package tech.codingzen.ktuple

data class KTuple7<out P0, out P1, out P2, out P3, out P4, out P5, out P6>(
  val part0: P0,
  val part1: P1,
  val part2: P2,
  val part3: P3,
  val part4: P4,
  val part5: P5,
  val part6: P6
)

fun <P0, P1, P2, P3, P4, P5, P6, Q0> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set0(q0: Q0):
  KTuple7<Q0, P1, P2, P3, P4, P5, P6> = KTuple7(q0, part1, part2, part3, part4, part5, part6)

fun <P0, P1, P2, P3, P4, P5, P6, Q1> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set1(q1: Q1):
  KTuple7<P0, Q1, P2, P3, P4, P5, P6> = KTuple7(part0, q1, part2, part3, part4, part5, part6)

fun <P0, P1, P2, P3, P4, P5, P6, Q2> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set2(q2: Q2):
  KTuple7<P0, P1, Q2, P3, P4, P5, P6> = KTuple7(part0, part1, q2, part3, part4, part5, part6)

fun <P0, P1, P2, P3, P4, P5, P6, Q3> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set3(q3: Q3):
  KTuple7<P0, P1, P2, Q3, P4, P5, P6> = KTuple7(part0, part1, part2, q3, part4, part5, part6)

fun <P0, P1, P2, P3, P4, P5, P6, Q4> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set4(q4: Q4):
  KTuple7<P0, P1, P2, P3, Q4, P5, P6> = KTuple7(part0, part1, part2, part3, q4, part5, part6)

fun <P0, P1, P2, P3, P4, P5, P6, Q5> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set5(q5: Q5):
  KTuple7<P0, P1, P2, P3, P4, Q5, P6> = KTuple7(part0, part1, part2, part3, part4, q5, part6)

fun <P0, P1, P2, P3, P4, P5, P6, Q6> KTuple7<P0, P1, P2, P3, P4, P5, P6>.set6(q6: Q6):
  KTuple7<P0, P1, P2, P3, P4, P5, Q6> = KTuple7(part0, part1, part2, part3, part4, part5, q6)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q0> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map0(fn: (P0) -> Q0): KTuple7<Q0, P1, P2, P3, P4, P5, P6> = KTuple7(
  fn(part0), part1, part2,
  part3, part4, part5, part6
)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q1> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map1(fn: (P1) -> Q1): KTuple7<P0, Q1, P2, P3, P4, P5, P6> = KTuple7(
  part0, fn(part1), part2,
  part3, part4, part5, part6
)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q2> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map2(fn: (P2) -> Q2): KTuple7<P0, P1, Q2, P3, P4, P5, P6> = KTuple7(
  part0, part1, fn(part2),
  part3, part4, part5, part6
)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q3> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map3(fn: (P3) -> Q3): KTuple7<P0, P1, P2, Q3, P4, P5, P6> = KTuple7(
  part0, part1, part2,
  fn(part3), part4, part5, part6
)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q4> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map4(fn: (P4) -> Q4): KTuple7<P0, P1, P2, P3, Q4, P5, P6> = KTuple7(
  part0, part1, part2,
  part3, fn(part4), part5, part6
)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q5> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map5(fn: (P5) -> Q5): KTuple7<P0, P1, P2, P3, P4, Q5, P6> = KTuple7(
  part0, part1, part2,
  part3, part4, fn(part5), part6
)

inline fun <P0, P1, P2, P3, P4, P5, P6, Q6> KTuple7<P0, P1, P2, P3, P4, P5,
  P6>.map6(fn: (P6) -> Q6): KTuple7<P0, P1, P2, P3, P4, P5, Q6> = KTuple7(
  part0, part1, part2,
  part3, part4, part5, fn(part6)
)
