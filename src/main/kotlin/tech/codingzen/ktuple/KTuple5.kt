package tech.codingzen.ktuple

data class KTuple5<out P0, out P1, out P2, out P3, out P4>(
  val part0: P0,
  val part1: P1,
  val part2: P2,
  val part3: P3,
  val part4: P4
)

fun <P0, P1, P2, P3, P4, Q0> KTuple5<P0, P1, P2, P3, P4>.set0(q0: Q0): KTuple5<Q0, P1, P2,
  P3, P4> = KTuple5(q0, part1, part2, part3, part4)

fun <P0, P1, P2, P3, P4, Q1> KTuple5<P0, P1, P2, P3, P4>.set1(q1: Q1): KTuple5<P0, Q1, P2,
  P3, P4> = KTuple5(part0, q1, part2, part3, part4)

fun <P0, P1, P2, P3, P4, Q2> KTuple5<P0, P1, P2, P3, P4>.set2(q2: Q2): KTuple5<P0, P1, Q2,
  P3, P4> = KTuple5(part0, part1, q2, part3, part4)

fun <P0, P1, P2, P3, P4, Q3> KTuple5<P0, P1, P2, P3, P4>.set3(q3: Q3): KTuple5<P0, P1, P2,
  Q3, P4> = KTuple5(part0, part1, part2, q3, part4)

fun <P0, P1, P2, P3, P4, Q4> KTuple5<P0, P1, P2, P3, P4>.set4(q4: Q4): KTuple5<P0, P1, P2,
  P3, Q4> = KTuple5(part0, part1, part2, part3, q4)

inline fun <P0, P1, P2, P3, P4, Q0> KTuple5<P0, P1, P2, P3, P4>.map0(fn: (P0) -> Q0):
  KTuple5<Q0, P1, P2, P3, P4> = KTuple5(fn(part0), part1, part2, part3, part4)

inline fun <P0, P1, P2, P3, P4, Q1> KTuple5<P0, P1, P2, P3, P4>.map1(fn: (P1) -> Q1):
  KTuple5<P0, Q1, P2, P3, P4> = KTuple5(part0, fn(part1), part2, part3, part4)

inline fun <P0, P1, P2, P3, P4, Q2> KTuple5<P0, P1, P2, P3, P4>.map2(fn: (P2) -> Q2):
  KTuple5<P0, P1, Q2, P3, P4> = KTuple5(part0, part1, fn(part2), part3, part4)

inline fun <P0, P1, P2, P3, P4, Q3> KTuple5<P0, P1, P2, P3, P4>.map3(fn: (P3) -> Q3):
  KTuple5<P0, P1, P2, Q3, P4> = KTuple5(part0, part1, part2, fn(part3), part4)

inline fun <P0, P1, P2, P3, P4, Q4> KTuple5<P0, P1, P2, P3, P4>.map4(fn: (P4) -> Q4):
  KTuple5<P0, P1, P2, P3, Q4> = KTuple5(part0, part1, part2, part3, fn(part4))
