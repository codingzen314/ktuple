package tech.codingzen.ktuple

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

class KTuple1Test {
  @Test
  fun test_set0() {
    val tuple = KTuple("a")
    assertThat(tuple.set0("foo")).isEqualTo(KTuple("foo"))
  }

  @Test
  fun test_map0() {
    val tuple = KTuple("a")
    assertThat(tuple.map0{ it.toUpperCase() }).isEqualTo(KTuple("A"))
  }
}
