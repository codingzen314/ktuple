package tech.codingzen.ktuple

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

class KTuple7Test {
  @Test
  fun test_set0(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set0("foo")).isEqualTo(KTuple("foo", 100, 'c', 21L, 1.toByte(), 2.71, false))
  }

  @Test
  fun test_set1(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set1("foo")).isEqualTo(KTuple("a", "foo", 'c', 21L, 1.toByte(), 2.71, false))
  }

  @Test
  fun test_set2(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set2("foo")).isEqualTo(KTuple("a", 100, "foo", 21L, 1.toByte(), 2.71, false))
  }

  @Test
  fun test_set3(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set3("foo")).isEqualTo(KTuple("a", 100, 'c', "foo", 1.toByte(), 2.71, false))
  }

  @Test
  fun test_set4(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set4("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, "foo", 2.71, false))
  }

  @Test
  fun test_set5(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set5("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), "foo", false))
  }

  @Test
  fun test_set6(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.set6("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, "foo"))
  }

  @Test
  fun test_map0(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map0{ it.toUpperCase() }).isEqualTo(KTuple("A", 100, 'c', 21L, 1.toByte(),
        2.71, false))
  }

  @Test
  fun test_map1(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map1{ it + 23 }).isEqualTo(KTuple("a", 123, 'c', 21L, 1.toByte(), 2.71, false))
  }

  @Test
  fun test_map2(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map2{ it.toUpperCase() }).isEqualTo(KTuple("a", 100, 'C', 21L, 1.toByte(),
        2.71, false))
  }

  @Test
  fun test_map3(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map3{ "a" }).isEqualTo(KTuple("a", 100, 'c', "a", 1.toByte(), 2.71, false))
  }

  @Test
  fun test_map4(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map4{ it + 1 }).isEqualTo(KTuple("a", 100, 'c', 21L, 2, 2.71, false))
  }

  @Test
  fun test_map5(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map5{ "a" }).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), "a", false))
  }

  @Test
  fun test_map6(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false)
    assertThat(tuple.map6{ true }).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, true))
  }
}
