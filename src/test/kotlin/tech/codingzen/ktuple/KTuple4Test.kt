package tech.codingzen.ktuple

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

class KTuple4Test {
  @Test
  fun test_set0(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.set0("foo")).isEqualTo(KTuple("foo", 100, 'c', 21L))
  }

  @Test
  fun test_set1(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.set1("foo")).isEqualTo(KTuple("a", "foo", 'c', 21L))
  }

  @Test
  fun test_set2(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.set2("foo")).isEqualTo(KTuple("a", 100, "foo", 21L))
  }

  @Test
  fun test_set3(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.set3("foo")).isEqualTo(KTuple("a", 100, 'c', "foo"))
  }

  @Test
  fun test_map0(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.map0{ it.toUpperCase() }).isEqualTo(KTuple("A", 100, 'c', 21L))
  }

  @Test
  fun test_map1(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.map1{ it + 23 }).isEqualTo(KTuple("a", 123, 'c', 21L))
  }

  @Test
  fun test_map2(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.map2{ it.toUpperCase() }).isEqualTo(KTuple("a", 100, 'C', 21L))
  }

  @Test
  fun test_map3(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L)
    assertThat(tuple.map3{ "a" }).isEqualTo(KTuple("a", 100, 'c', "a"))
  }
}
