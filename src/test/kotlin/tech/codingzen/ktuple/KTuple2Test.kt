package tech.codingzen.ktuple

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

class KTuple2Test {
  @Test
  fun test_set0(): Unit {
    val tuple = KTuple("a", 100)
    assertThat(tuple.set0("foo")).isEqualTo(KTuple("foo", 100))
  }

  @Test
  fun test_set1(): Unit {
    val tuple = KTuple("a", 100)
    assertThat(tuple.set1("foo")).isEqualTo(KTuple("a", "foo"))
  }

  @Test
  fun test_map0(): Unit {
    val tuple = KTuple("a", 100)
    assertThat(tuple.map0{ it.toUpperCase() }).isEqualTo(KTuple("A", 100))
  }

  @Test
  fun test_map1(): Unit {
    val tuple = KTuple("a", 100)
    assertThat(tuple.map1{ it + 23 }).isEqualTo(KTuple("a", 123))
  }
}
