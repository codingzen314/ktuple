package tech.codingzen.ktuple

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import assertk.assertions.isFalse
import assertk.assertions.isInstanceOf
import assertk.assertions.isTrue
import kotlin.Unit
import org.junit.jupiter.api.Test
import tech.codingzen.ktuple.KTuple8
import tech.codingzen.ktuple.map0
import tech.codingzen.ktuple.map1
import tech.codingzen.ktuple.map2
import tech.codingzen.ktuple.map3
import tech.codingzen.ktuple.map4
import tech.codingzen.ktuple.map5
import tech.codingzen.ktuple.map6
import tech.codingzen.ktuple.map7
import tech.codingzen.ktuple.set0
import tech.codingzen.ktuple.set1
import tech.codingzen.ktuple.set2
import tech.codingzen.ktuple.set3
import tech.codingzen.ktuple.set4
import tech.codingzen.ktuple.set5
import tech.codingzen.ktuple.set6
import tech.codingzen.ktuple.set7

class KTuple8Test {
  @Test
  fun test_set0(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set0("foo")).isEqualTo(KTuple("foo", 100, 'c', 21L, 1.toByte(), 2.71, false,
        "B"))
  }

  @Test
  fun test_set1(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set1("foo")).isEqualTo(KTuple("a", "foo", 'c', 21L, 1.toByte(), 2.71, false,
        "B"))
  }

  @Test
  fun test_set2(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set2("foo")).isEqualTo(KTuple("a", 100, "foo", 21L, 1.toByte(), 2.71, false,
        "B"))
  }

  @Test
  fun test_set3(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set3("foo")).isEqualTo(KTuple("a", 100, 'c', "foo", 1.toByte(), 2.71, false,
        "B"))
  }

  @Test
  fun test_set4(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set4("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, "foo", 2.71, false, "B"))
  }

  @Test
  fun test_set5(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set5("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), "foo", false,
        "B"))
  }

  @Test
  fun test_set6(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set6("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, "foo",
        "B"))
  }

  @Test
  fun test_set7(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.set7("foo")).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false,
        "foo"))
  }

  @Test
  fun test_map0(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map0{ it.toUpperCase() }).isEqualTo(KTuple("A", 100, 'c', 21L, 1.toByte(),
        2.71, false, "B"))
  }

  @Test
  fun test_map1(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map1{ it + 23 }).isEqualTo(KTuple("a", 123, 'c', 21L, 1.toByte(), 2.71, false,
        "B"))
  }

  @Test
  fun test_map2(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map2{ it.toUpperCase() }).isEqualTo(KTuple("a", 100, 'C', 21L, 1.toByte(),
        2.71, false, "B"))
  }

  @Test
  fun test_map3(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map3{ "a" }).isEqualTo(KTuple("a", 100, 'c', "a", 1.toByte(), 2.71, false,
        "B"))
  }

  @Test
  fun test_map4(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map4{ it + 1 }).isEqualTo(KTuple("a", 100, 'c', 21L, 2, 2.71, false,
        "B"))
  }

  @Test
  fun test_map5(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map5{ "a" }).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), "a", false, "B"))
  }

  @Test
  fun test_map6(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map6{ true }).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, true,
        "B"))
  }

  @Test
  fun test_map7(): Unit {
    val tuple = KTuple("a", 100, 'c', 21L, 1.toByte(), 2.71, false, "B")
    assertThat(tuple.map7{ it.toLowerCase() }).isEqualTo(KTuple("a", 100, 'c', 21L, 1.toByte(),
        2.71, false, "b"))
  }
}
